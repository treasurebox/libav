CURRENTPATH=`pwd`
PLATFORM=osx
#SDKVERSION=7.0
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"


mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib"

#ARCHS='i386 arm'
ARCHS=x86_64

for ARCH in $ARCHS 
do
	make distclean 2>/dev/null
	
	echo "Please stand by... (compiler at $CROSS_TOP) (sdk at $CROSS_SDK)"

	

	#export CFLAGS="-arch armv7 -isysroot $SYSROOT"
	#export LDFLAGS="-ios_version_min 5.1 -syslibroot $SYSROOT -lcrt1.o -lc"

	mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
	LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-openssl-${VERSION}.log"

	#needed for preprocessor on ARM

	#--disable-asm is needed on ios... even --disable-inline-asm does not suffice

	#echo configure for cross compiling...
	# 300kb but gives error when parsing header...	
	#./configure --disable-everything --disable-programs --enable-demuxer=mpegvideo --enable-decoder=aac --enable-decoder=aac_latm --enable-shared --prefix=${CURRENTPATH}/lib/$ARCH --enable-cross-compile --arch=armel --target-os=linux --cross-prefix=${CROSS_TOP}/bin/arm-linux-gnueabi- --sysroot=$SYSROOT
	# very large (60mb!), should parse everything imaginable
	#./configure --disable-programs --disable-shared --enable-static --prefix=${CURRENTPATH}/lib/$ARCH --enable-cross-compile --arch=arm --target-os=darwin --cc="$CC" --ld=$"LD"
	#smallish (5000kb), working for ac3 audio from youtube
	#./configure --disable-everything --disable-programs --enable-demuxer=mov --enable-decoder=aac --enable-decoder=aac_latm --enable-demuxer=mp3 --enable-decoder=mp3 --enable-decoder=mp3adu --enable-decoder=mp3adufloat --enable-decoder=mp3float --enable-decoder=mp3on4 --enable-decoder=mp3on4float --enable-shared --disable-static --prefix=${CURRENTPATH}/lib/$ARCH
	./configure --disable-everything --disable-programs --enable-demuxer=mov --enable-decoder=aac --enable-decoder=aac_latm --enable-avresample --enable-shared --disable-static --prefix=${CURRENTPATH}/lib/$ARCH
	
	
	echo start build 
	make && make install
done
