CURRENTPATH=`pwd`
PLATFORM=iPhoneOS
SDKVERSION=7.0
mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"


mkdir -p "${CURRENTPATH}/bin"
mkdir -p "${CURRENTPATH}/lib"

#ARCHS='i386 arm'
ARCHS=arm
DEVELOPER=`xcode-select -print-path`

export CROSS_BASE="${DEVELOPER}"
export CROSS_TOP="${DEVELOPER}/Platforms/${PLATFORM}.platform/Developer"
export CROSS_SDK="${PLATFORM}${SDKVERSION}.sdk"

for ARCH in $ARCHS 
do
	make distclean 2>/dev/null
	
	echo "Please stand by... (compiler at $CROSS_TOP) (sdk at $CROSS_SDK)"

	export CPP="xcrun --sdk iphoneos cpp"
	export CC="${CROSS_BASE}/usr/bin/gcc -arch armv7"
	export AS="xcrun --sdk iphoneos as"
	export RANLIB="xcrun --sdk iphoneos ranlib"
	export AR="xcrun --sdk iphoneos ar"
	export LD="${CROSS_BASE}/usr/bin/ld -arch armv7"
	export NM="xcrun --sdk iphoneos nm"  
	#no libtool!
	export LIBTOOL="xcrun --sdk iphoneos libtool" 

	export SYSROOT="${CROSS_TOP}/SDKs/${CROSS_SDK}"
	echo "system root: $SYSROOT"
	

	export CFLAGS="-arch armv7 -isysroot $SYSROOT"
	export LDFLAGS="-ios_version_min 5.1 -syslibroot $SYSROOT -lcrt1.o -lc"

	mkdir -p "${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk"
	LOG="${CURRENTPATH}/bin/${PLATFORM}${SDKVERSION}-${ARCH}.sdk/build-openssl-${VERSION}.log"

	#--disable-asm is needed on ios... even --disable-inline-asm does not suffice

	echo configure for cross compiling...
	# 300kb but gives error when parsing header...	
	#./configure --disable-everything --disable-programs --enable-demuxer=mpegvideo --enable-decoder=aac --enable-decoder=aac_latm --enable-shared --prefix=${CURRENTPATH}/lib/$ARCH --enable-cross-compile --arch=armel --target-os=linux --cross-prefix=${CROSS_TOP}/bin/arm-linux-gnueabi- --sysroot=$SYSROOT
	# very large (60mb!), should parse everything imaginable
	#./configure --disable-programs --disable-shared --enable-static --prefix=${CURRENTPATH}/lib/$ARCH --enable-cross-compile --arch=arm --target-os=darwin --cc="$CC" --ld=$"LD"
	#smallish (5000kb), working for ac3 audio from youtube
	./configure --disable-everything --disable-programs --enable-demuxer=mov --enable-decoder=aac --enable-decoder=aac_latm --disable-shared --enable-static --prefix=${CURRENTPATH}/lib/$ARCH --enable-cross-compile --arch=arm --target-os=darwin --cc="$CC" --ld=$"LD" --disable-asm
	
	# might not be necessary for ios
	# --cross-prefix=${CROSS_TOP}/bin/arm-linux-gnueabi- 


	#needed for preprocessor on ARM

	echo "#undef HAVE_TRUNC" >>config.h
	echo "#undef HAVE_TRUNCF" >>config.h
	echo "#undef HAVE_LRINT" >>config.h
	echo "#undef HAVE_LRINTF" >>config.h
	echo "#undef HAVE_ROUND" >>config.h
	echo "#undef HAVE_ROUNDF" >>config.h
	echo "#undef HAVE_RINTF" >>config.h
	echo "#undef HAVE_RINT" >>config.h
	echo "#undef HAVE_ISINF" >>config.h
	echo "#undef HAVE_ISINFF" >>config.h
	echo "#undef HAVE_CBRTF" >>config.h
	echo "#undef HAVE_ISNAN" >>config.h
	echo "#undef HAVE_ISNANF" >>config.h
	
	
	echo "#define HAVE_TRUNC 1" >>config.h
	echo "#define HAVE_TRUNCF 1" >>config.h
	echo "#define HAVE_LRINT 1" >>config.h
	echo "#define HAVE_LRINTF 1" >>config.h
	echo "#define HAVE_ROUND 1" >>config.h
	echo "#define HAVE_ROUNDF 1" >>config.h
	echo "#define HAVE_RINTF 1" >>config.h
	echo "#define HAVE_RINT 1" >>config.h
	echo "#define HAVE_ISINF 1" >>config.h
	echo "#define HAVE_ISINFF 1" >>config.h
	echo "#define HAVE_CBRTF 1" >>config.h
	echo "#define HAVE_ISNAN 1" >>config.h
	echo "#define HAVE_ISNANF 1" >>config.h

	
	echo start build 
	make && make install
done
